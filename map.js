function map(items, callback) {
    if (Array.isArray(items)) {
        let mapValues = []
        for (let i = 0; i < items.length; i++) {
            let returnValue = callback(items[i], i, items);
            mapValues.push(returnValue)
        }
        return mapValues

    } else {

        if (items.length === 0) {
            console.log("Empty Array")
        } else {
            console.log("Not Iterable")
        }

    }
};



module.exports = map;