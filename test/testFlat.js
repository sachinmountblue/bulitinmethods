const flatten = require("../flat.js");
const nestedArray = [
    [1, [2],
        [
            [3]
        ],
        [
            [
                [4]
            ]
        ]
    ]
];
console.log(flatten(nestedArray))