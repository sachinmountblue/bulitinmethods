let newArr = []

function flatten(nestedArray) {

    if (nestedArray.length === 0) {
        return []
    }

    for (let i = 0; i < nestedArray.length; i++) {
        if (!Array.isArray(nestedArray[i])) {
            newArr.push(nestedArray[i])
        } else {
            flatten(nestedArray[i])
        }


    }
    return newArr
}


module.exports = flatten