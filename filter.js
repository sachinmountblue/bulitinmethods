 function filter(items, callback) {
   if (Array.isArray(items) && items.length != 0) {
     let returnArray = []
     for (let i = 0; i < items.length; i++) {

       returnValue = callback(items[i]);
       if (returnValue != 'undefined') {
         returnArray.push(returnValue)
       }


     }
     if (returnArray.length == 0) {
       return []
     } else {
       return returnArray
     }


   } else {
     if (items.length === 0) {
       console.log("Empty Array")
     } else {
       console.log("Not Iterable")
     }



   }
 };


 module.exports = filter